package com.example.administrator.thikana.activity

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import com.example.administrator.thikana.R
import com.example.administrator.thikana.SearchActivity
import com.example.administrator.thikana.retrofit.RepositoryProvider
import com.example.administrator.thikana.utils.AppUtils
import com.example.administrator.thikana.utils.MyDialog
import io.paperdb.Paper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_login.*

/**
 * Created by administrator on 20/3/18.
 */
class LoginActivity : Activity() {

    val compositeDisposable: CompositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login)

        btn_login.setOnClickListener {

            var email = et_email.text.toString()
            var pwd = et_pwd.text.toString()

            if (validate(email, pwd)) {
                if(AppUtils().isNetworkAvailable(this)) {
                    callLoginApi(email, pwd)
                }
                else{
                    Toast.makeText(this,"No Internet Connection",Toast.LENGTH_SHORT).show()
                }
            }
        }

        et_email.setOnFocusChangeListener(View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus && et_email.text.toString().isEmpty())
                resetAllErrors()
        })

        et_pwd.setOnFocusChangeListener(View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus && et_pwd.text.toString().isEmpty())
                resetAllErrors()
        })

    }

    fun validate(email: String, pwd: String): Boolean {
        resetAllErrors()
        var is_error: Boolean = false

        if (email.toString().trim().length == 0) {
            is_error = true
            til_email.isErrorEnabled = true
            til_email.error=getString(R.string.username_cannot_be_empty)
            //til_email.setError(resources.getString(R.string.username_cannot_be_empty))
            //til_email.solidColor
            //til_email.setBackgroundColor(Color.WHITE)

        }
        if (pwd.toString().trim().length == 0) {
            is_error = true
            til_pwd.isErrorEnabled = true
            til_pwd.error=getString(R.string.password_cannot_be_blank)
            //til_pwd.setError(resources.getString(R.string.password_cannot_be_blank))
            //til_pwd.setBackgroundColor(Color.WHITE)
        }
        if (!isValidEmail(email.toString())) {
            is_error = true
            til_email.error= getString(R.string.please_enter_valid_email)
        }

        if (!is_error) {
            resetAllErrors()
            return true
        } else {
            return false
        }
    }


    fun callLoginApi(email: String, pwd: String) {
        val mDialog:MyDialog= MyDialog()
        mDialog.showProgressLoader(this,"Login..")
        val repository = RepositoryProvider.provideRepository()

        compositeDisposable.add(
                repository.loginUsers(email, pwd)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ result ->
                            //                            Log.d("Result", " ${result} Login")
                            if (result.success.toBoolean()) {
                               mDialog.dismissProgressLoader()
                                Paper.book().write("isLogin",true)
                                val intent = Intent(this@LoginActivity, SearchActivity::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                startActivity(intent)
                                finish()
                            } else {
                                mDialog.dismissProgressLoader()
//                                Toast.makeText(this, result.message, Toast.LENGTH_LONG).show()
                                Toast.makeText(this,"Invalid UserName/Password", Toast.LENGTH_LONG).show()
                            }

                        }, { error ->
                            error.printStackTrace()
                        })
        )
    }

    fun resetAllErrors() {
        til_email.isErrorEnabled = false
        til_pwd.isErrorEnabled = false
    }

    fun isValidEmail(target: CharSequence): Boolean {
        return if (TextUtils.isEmpty(target)) {
            false
        } else {
            android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }
    }

    override fun onDestroy() {
        compositeDisposable.clear()
        super.onDestroy()
    }
}