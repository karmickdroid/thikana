package com.example.administrator.thikana.activity

import android.app.Dialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.media.MediaBrowserCompat
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.widget.ImageView
import com.example.administrator.thikana.R
import com.example.administrator.thikana.adapter.UserDetailsAdapter
import com.example.administrator.thikana.datamodel.UserDetails
import com.example.administrator.thikana.retrofit.AppConstant
import com.example.administrator.thikana.utils.MyDialog

import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.squareup.picasso.Picasso
import io.paperdb.Paper
import kotlinx.android.synthetic.main.activity_details.*
import kotlinx.android.synthetic.main.my_toolbar.*

class UserDetailsMapsActivity : AppCompatActivity(), OnMapReadyCallback, MyDialog.LogoutListener {
    override fun onOkClickListener() {
        callLogout()
    }

    private lateinit var mMap: GoogleMap
    lateinit var userData_value: UserDetails
    var userData_head: UserDetails = UserDetails()
    var isImageFitToScreen: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        Paper.init(this)

        userData_value = intent.extras.get("userDetailsValue") as UserDetails

        populateDataToRecyclerView()
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        tv_name.text = userData_value.full_name
        tv_address.text = userData_value.nearest_landmark

        var imgUrl: String = AppConstant().baseUrl+"public/uploads/homeless_people/borough_" + userData_value.borough_no + "/photo/ward_" + userData_value.ward_no + "/" + userData_value.photo_1 + ".jpg"
        Log.v("imgUrl", "::" + imgUrl)

        Picasso.with(this)
                .load(imgUrl)
                .placeholder(R.drawable.square_user)
                .error(R.drawable.square_user)
                .into(iv_user)

        iv_user.setOnClickListener {
            val dialog = Dialog(this, R.style.AppTheme)

                dialog.setContentView(R.layout.frame_fullscreen)
                val iv_photo = dialog.findViewById(R.id.iv_photo) as ImageView
                val iv_cross = dialog.findViewById(R.id.iv_cross) as ImageView
                Picasso.with(this)
                        .load(imgUrl)
                        .placeholder(R.drawable.square_user)
                        .error(R.drawable.square_user)
                        .into(iv_photo)

                iv_cross.setOnClickListener {
                    dialog.dismiss()
                }
                dialog.show()

        }
    }

    private fun populateDataToRecyclerView() {

        val headArrayList = arrayOf(
                userData_head.nearest_landmark,
                userData_head.ward_no,
                userData_head.father_husband_name,
                userData_head.present_living_status_desc,
                userData_head.reasons_of_migration,
                userData_head.name_of_municipality,
                userData_head.form_no,
                userData_head.present_living_status,
                userData_head.place_of_origin,

                userData_head.name_of_streetlocation,
                userData_head.present_living_status_description,
                userData_head.living_in_static_location,
                userData_head.living_since,
                userData_head.whether_bplvoter_aadharany_photo_id_card_holder_govt_issued,
                userData_head.id_card_number,
                userData_head.whether_any_type_of_ration_card_holder,
                userData_head.ration_card_number,
                userData_head.whether_willing_to_avail_of_govt_arranged_shelter,
                userData_head.full_name_of_respondent,
                userData_head.relationship_with_respondent,
                userData_head.age,
                userData_head.sex,
                userData_head.marital_status,
                userData_head.physically_challenged,
                userData_head.mentally_challenged,
                userData_head.litarate,
                userData_head.educational_qualification,
                userData_head.occupation,
                userData_head.monthly_income,
                userData_head.personal_identification_mark,
                userData_head.place_of_occupation,
                userData_head.habits,
                userData_head.reason_for_stay_in_kmc,
                userData_head.zone_no,
                userData_head.homeless_status)

        val valueArrayList = arrayOf(userData_value.nearest_landmark,
                userData_value.ward_no,
                userData_value.father_husband_name,
                userData_value.present_living_status_desc,
                userData_value.reasons_of_migration,
                userData_value.name_of_municipality,
                userData_value.form_no,
                userData_value.present_living_status,
                userData_value.place_of_origin,

                userData_value.name_of_streetlocation,
                userData_value.present_living_status_description,
                userData_value.living_in_static_location,
                userData_value.living_since,
                userData_value.whether_bplvoter_aadharany_photo_id_card_holder_govt_issued,
                userData_value.id_card_number,
                userData_value.whether_any_type_of_ration_card_holder,
                userData_value.ration_card_number,
                userData_value.whether_willing_to_avail_of_govt_arranged_shelter,
                userData_value.full_name_of_respondent,
                userData_value.relationship_with_respondent,
                userData_value.age,
                userData_value.sex,
                userData_value.marital_status,
                userData_value.physically_challenged,
                userData_value.mentally_challenged,
                userData_value.litarate,
                userData_value.educational_qualification,
                userData_value.occupation,
                userData_value.monthly_income,
                userData_value.personal_identification_mark,
                userData_value.place_of_occupation,
                userData_value.habits,
                userData_value.reason_for_stay_in_kmc,
                userData_value.zone_no,
                userData_value.homeless_status)


        //adding a layoutmanager
        recyclerVw.layoutManager = GridLayoutManager(this, 2)
//        recyclerVw.addItemDecoration(LinearLayoutSpaceItemDecoration(this))

        //creating our listAdapter
        var adapter = UserDetailsAdapter(this, headArrayList, valueArrayList, valueArrayList.size)


        //now adding the listAdapter to recyclerview
        recyclerVw.adapter = adapter

        ivBackToolbar.setOnClickListener {
            onBackPressed()
        }
        ivLogoutToolbar.setOnClickListener {
            val mDialog: MyDialog = MyDialog()
            mDialog.logoutPopup(this, "Logout", this)
        }
    }

    private fun callLogout() {
        Paper.book().delete("isLogin")
        val i = Intent(this, LoginActivity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(i)
        this.finishAffinity()

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Add a marker in Sydney and move the camera
        val sydney = LatLng(userData_value.latitude.toDouble(), userData_value.longitude.toDouble())
        mMap.addMarker(MarkerOptions().position(sydney).title("Marker in Location"))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 14F))
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }
}
