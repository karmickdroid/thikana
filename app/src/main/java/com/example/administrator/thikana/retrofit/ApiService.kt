package com.example.administrator.thikana.retrofit

import com.example.administrator.thikana.response.*


/**
 * Created by administrator on 20/3/18.
 */
interface ApiService {

    //Login Api
    @retrofit2.http.POST("get-login-access-api")
    fun login(@retrofit2.http.Query("email") email: String,
              @retrofit2.http.Query("password") password: String): io.reactivex.Observable<LoginResult>

    //Search Filter Api
    @retrofit2.http.GET("get-all-drop-down-api")
    fun searchFilterApi(): io.reactivex.Observable<SearchFilter>

    //UserList
    @retrofit2.http.POST("get-account-api")
    fun userList(@retrofit2.http.Query("name_of_municipality") name_of_municipality: String,
                 @retrofit2.http.Query("ward_no") ward_no: String,
                 @retrofit2.http.Query("name_of_streetlocation") name_of_streetlocation: String,
                 @retrofit2.http.Query("nearest_landmark") nearest_landmark: String,
                 @retrofit2.http.Query("full_name_of_respondent") full_name_of_respondent: String,
                 @retrofit2.http.Query("present_living_status") present_living_status: String,
                 @retrofit2.http.Query("place_of_origin") place_of_origin: String,
                 @retrofit2.http.Query("reasons_of_migration_from_place_of_origin") reasons_of_migration_from_place_of_origin: String,
                 @retrofit2.http.Query("whether_bplvoter_aadharany_photo_id_card_holder_govt_issued") whether_bplvoter_aadharany_photo_id_card_holder_govt_issued: String,
                 @retrofit2.http.Query("whether_willing_to_avail_of_govt_arranged_shelter") whether_willing_to_avail_of_govt_arranged_shelter: String,
                 @retrofit2.http.Query("full_name") full_name: String,
                 @retrofit2.http.Query("offset") offset: Int=0,
                 @retrofit2.http.Query("length") length: Int=10): io.reactivex.Observable<UserList>

    /**
     * Companion object for the factory
     */
    companion object Factory {

        fun create(): ApiService {
            val retrofit = retrofit2.Retrofit.Builder()
                    .addCallAdapterFactory(retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory.create())
                    .addConverterFactory(retrofit2.converter.gson.GsonConverterFactory.create())
//                    .baseUrl("http://karmickproduction.com/thikana/admin/")
                    .baseUrl(AppConstant().baseUrl)
                    .build()

            return retrofit.create(ApiService::class.java);
        }
    }
}