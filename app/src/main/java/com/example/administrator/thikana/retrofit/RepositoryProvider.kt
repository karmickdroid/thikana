package com.example.administrator.thikana.retrofit

/**
 * Created by administrator on 20/3/18.
 */
object RepositoryProvider {

    fun provideRepository(): Repository {
        return Repository(ApiService.create())
    }
}