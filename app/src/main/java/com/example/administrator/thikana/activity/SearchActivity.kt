package com.example.administrator.thikana

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.Parcelable
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import com.example.administrator.thikana.activity.LoginActivity
import com.example.administrator.thikana.activity.UserListActivity
import com.example.administrator.thikana.datamodel.SearchData
import io.paperdb.Paper
import com.example.administrator.thikana.response.*
import com.example.administrator.thikana.retrofit.RepositoryProvider
import com.example.administrator.thikana.utils.AppUtils
import com.example.administrator.thikana.utils.MyDialog
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.my_toolbar.*
import java.io.Serializable

/**
 * Created by Pratim on 21/3/18.
 */
class SearchActivity : Activity(), AdapterView.OnItemSelectedListener,MyDialog.LogoutListener  {
    override fun onOkClickListener() {
        callLogout()
    }

    val arrayMunicipality = ArrayList<String>()//arrayOf("Select Municipality")
    val arrayLivingStatus = ArrayList<String>()//arrayOf("Select")
    val arrayPhotoID = ArrayList<String>()//arrayOf("Select")
    val arrayWillingShelter = ArrayList<String>()//arrayOf("Select","Yes","No")
    val compositeDisposable: CompositeDisposable = CompositeDisposable()


    var municipalityArray: List<Municipality> = ArrayList<Municipality>()
    var presentLivingArray: List<PresentLiving> = ArrayList<PresentLiving>()
    var govtShelterArray: List<GovtShelter> = ArrayList<GovtShelter>()
    var whetherAnycardArray: List<WhetherAnycard> = ArrayList<WhetherAnycard>()

    var spMunicipality_code: String = ""
    var spLivingStatus_code: String = ""
    var spPhotoID_code: String = ""
    var spWillingforShelter_code: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        Paper.init(this)
        if(AppUtils().isNetworkAvailable(this))
        {
            callSearchFilterApi()
        }
        else{
            Toast.makeText(this,"No Internet Connection",Toast.LENGTH_SHORT).show()
        }
        initView()
    }

    private fun initView() {
        spMunicipality!!.setOnItemSelectedListener(this)
        spLivingStatus!!.setOnItemSelectedListener(this)
        spPhotoID!!.setOnItemSelectedListener(this)
        spWillingforShelter!!.setOnItemSelectedListener(this)
        initSpinner()
        setUpSpinner()

        tvSearchButton.setOnClickListener {
            redirectToUserList()
        }

        ivBackToolbar.setOnClickListener {
            this.finish()
        }
    }

    private fun redirectToUserList() {

        var searchData: SearchData = SearchData()
        searchData.name_of_municipality = spMunicipality_code
        searchData.ward_no = etWordNo.text.toString()
        searchData.name_of_streetlocation = etLocationStreet.text.toString()
        searchData.nearest_landmark = etNearestLandmark.text.toString()
        searchData.full_name_of_respondent = etName.text.toString()
        searchData.present_living_status = spLivingStatus_code
        searchData.place_of_origin = spOrigin.text.toString()
        searchData.reasons_of_migration_from_place_of_origin = etMigrationReason.text.toString()
        searchData.whether_bplvoter_aadharany_photo_id_card_holder_govt_issued = spPhotoID_code
        searchData.whether_willing_to_avail_of_govt_arranged_shelter = spWillingforShelter_code
        searchData.full_name = etName.text.toString()

        val intent = Intent(this, UserListActivity::class.java)
        intent.putExtra("searchData", searchData as Serializable)
        startActivity(intent)
    }

    private fun initSpinner() {
        arrayMunicipality.add("Select Municipality")
        arrayLivingStatus.add("Select")
        arrayPhotoID.add("Select")
        arrayWillingShelter.add("Select")
    }

    private fun setUpSpinner() {

        setupSpinner(spMunicipality, arrayMunicipality)
        setupSpinner(spPhotoID, arrayPhotoID)
        setupSpinner(spWillingforShelter, arrayWillingShelter)
        setupSpinner(spLivingStatus, arrayLivingStatus)


        ivBackToolbar.visibility = View.GONE

        ivLogoutToolbar.setOnClickListener {
            val mDialog: MyDialog = MyDialog()
            mDialog.logoutPopup(this, "Logout", this)
        }
    }

    private fun callLogout() {
        Paper.book().delete("isLogin")
        val i = Intent(this, LoginActivity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(i)
        this.finish()

    }

    private fun setupSpinner(spinner: Spinner?, arrayList: ArrayList<String>) {
        // Create an ArrayAdapter using a simple spinner layout and languages array
        val aa = ArrayAdapter(this, R.layout.spinner_text_layout, arrayList)
        // Set layout to use when the list of choices appear
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Set Adapter to Spinner
        spinner!!.setAdapter(aa)
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        val sp: Spinner = p0 as Spinner
        //p2=position
        if (sp.id == R.id.spMunicipality) {
            if (p2 != 0) {
                spMunicipality_code = municipalityArray[p2 - 1].municipality_code
            }
            else{
                spMunicipality_code=""
            }
        } else if (sp.id == R.id.spLivingStatus) {
            if (p2 != 0) {
                spLivingStatus_code = presentLivingArray[p2 - 1].present_living_code
            }
            else{
                spLivingStatus_code=""
            }
        } else if (sp.id == R.id.spPhotoID) {
            if (p2 != 0) {
                spPhotoID_code = whetherAnycardArray[p2 - 1].whether_anycard_code
            }
            else{
                spPhotoID_code=""
            }
        } else if (sp.id == R.id.spWillingforShelter) {
            if (p2 != 0) {
                spWillingforShelter_code = govtShelterArray[p2 - 1].govt_shelter_code
            }
            else{
                spWillingforShelter_code=""
            }
        }
    }

    private fun callSearchFilterApi() {
        val mDialog: MyDialog = MyDialog()
        mDialog.showProgressLoader(this, "Fetching..")

        val repository = RepositoryProvider.provideRepository()

        compositeDisposable.add(
                repository.searchFilter()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ result ->
                            mDialog.dismissProgressLoader()
                            Log.d("callSearchFilterApi", "" + result.toString());
                            populateData(result)
                        }, { error ->
                            mDialog.dismissProgressLoader()
                            error.printStackTrace()
                        })
        )
    }


    private fun populateData(result: SearchFilter) {

        arrayMunicipality.clear()
        arrayLivingStatus.clear()
        arrayPhotoID.clear()
        arrayWillingShelter.clear()

        initSpinner()
        //municipality
        municipalityArray = result.municipalityArr
        for (municipality in municipalityArray) {
            arrayMunicipality.add(municipality.municipality_name)
        }

        //living status
        presentLivingArray = result.presentLivingArr
        for (presentliv in presentLivingArray) {
            arrayLivingStatus.add(presentliv.present_living_name)
        }

        //photo id
        whetherAnycardArray = result.whetherAnycardArr
        for (photoid in whetherAnycardArray) {
            arrayPhotoID.add(photoid.whether_anycard_name)
        }

        //govt shelter
        govtShelterArray = result.govtShelterArr
        for (govSheltr in govtShelterArray) {
            arrayWillingShelter.add(govSheltr.govt_shelter_name)
        }
        setUpSpinner()
    }

}