package com.example.administrator.thikana.retrofit

import com.example.administrator.thikana.response.*


/**
 * Created by administrator on 20/3/18.
 */
class Repository(val apiService: ApiService) {

    fun loginUsers(email: String, password: String): io.reactivex.Observable<LoginResult> {
        return apiService.login(email, password)
    }

    fun getUsers(name_of_municipality: String,
                 ward_no: String,
                 name_of_streetlocation: String,
                 nearest_landmark: String,
                 full_name_of_respondent: String,
                 present_living_status: String,
                 place_of_origin: String,
                 reasons_of_migration_from_place_of_origin: String,
                 whether_bplvoter_aadharany_photo_id_card_holder_govt_issued: String,
                 whether_willing_to_avail_of_govt_arranged_shelter: String,
                 full_name: String,
                 offset: Int
                 , length: Int): io.reactivex.Observable<UserList> {

        return apiService.userList(name_of_municipality,
                ward_no,
                name_of_streetlocation,
                nearest_landmark,
                full_name_of_respondent,
                present_living_status,
                place_of_origin,
                reasons_of_migration_from_place_of_origin,
                whether_bplvoter_aadharany_photo_id_card_holder_govt_issued,
                whether_willing_to_avail_of_govt_arranged_shelter,
                full_name,
                offset
                , length)
    }

    fun searchFilter(): io.reactivex.Observable<SearchFilter> {
        return apiService.searchFilterApi()
    }
}