package com.example.administrator.thikana.adapter

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.administrator.thikana.R
import com.example.administrator.thikana.activity.UserDetailsMapsActivity
import com.example.administrator.thikana.activity.UserListActivity
import com.example.administrator.thikana.datamodel.UserData
import com.example.administrator.thikana.datamodel.UserDetails
import java.io.Serializable
import android.text.style.ForegroundColorSpan
import android.text.SpannableString
import android.text.SpannableStringBuilder
import android.util.Log
import com.squareup.picasso.Picasso
import android.widget.LinearLayout
import com.example.administrator.thikana.retrofit.AppConstant


/**
 * Created by administrator on 21/3/18.
 */
class UserListAdapter(val context: UserListActivity, val userList: ArrayList<UserData>) : RecyclerView.Adapter<UserListAdapter.ViewHolder>() {
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserListAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_layout_user, parent, false)
        return ViewHolder(v).listen { pos, type ->
            val userData: UserData = userList.get(pos)
            val userDetailsValue: UserDetails = UserDetails()

            userDetailsValue.nearest_landmark = userData.nearest_landmark
            userDetailsValue.ward_no = userData.ward_no
            userDetailsValue.father_husband_name = userData.fatherhusbands_name
            userDetailsValue.present_living_status_desc = userData.present_living_status_description
            userDetailsValue.reasons_of_migration = userData.reasons_of_migration_from_place_of_origin
            userDetailsValue.name_of_municipality = userData.name_of_municipality
            userDetailsValue.form_no = userData.form_no
            userDetailsValue.present_living_status = userData.present_living_status
            userDetailsValue.place_of_origin = userData.place_of_origin
            userDetailsValue.latitude = userData.latitude
            userDetailsValue.longitude = userData.longitude
            userDetailsValue.full_name = userData.full_name
            userDetailsValue.borough_no = userData.borough_no
            userDetailsValue.photo_1 = userData.photo_1

            userDetailsValue.name_of_streetlocation = userData.name_of_streetlocation
            userDetailsValue.present_living_status_description = userData.present_living_status_description
            userDetailsValue.living_in_static_location = userData.living_in_static_location
            userDetailsValue.living_since = userData.living_since
            userDetailsValue.whether_bplvoter_aadharany_photo_id_card_holder_govt_issued = userData.whether_bplvoter_aadharany_photo_id_card_holder_govt_issued
            userDetailsValue.id_card_number = userData.id_card_number
            userDetailsValue.whether_any_type_of_ration_card_holder = userData.whether_any_type_of_ration_card_holder
            userDetailsValue.ration_card_number = userData.ration_card_number
            userDetailsValue.whether_willing_to_avail_of_govt_arranged_shelter = userData.whether_willing_to_avail_of_govt_arranged_shelter
            userDetailsValue.full_name_of_respondent = userData.full_name_of_respondent
            userDetailsValue.relationship_with_respondent = userData.relationship_with_respondent
            userDetailsValue.age = userData.age
            userDetailsValue.sex = userData.sex

            userDetailsValue.marital_status = userData.marital_status
            userDetailsValue.physically_challenged = userData.physically_challenged

            userDetailsValue.mentally_challenged = userData.mentally_challenged
            userDetailsValue.litarate = userData.litarate
            userDetailsValue.educational_qualification = userData.educational_qualification
            userDetailsValue.occupation = userData.occupation
            userDetailsValue.monthly_income = userData.monthly_income
            userDetailsValue.personal_identification_mark = userData.personal_identification_mark
            userDetailsValue.place_of_occupation = userData.place_of_occupation
            userDetailsValue.habits = userData.habits

            userDetailsValue.reason_for_stay_in_kmc = userData.reason_for_stay_in_kmc
            userDetailsValue.zone_no = userData.zone_no
            userDetailsValue.homeless_status = userData.homeless_status


            userDetailsValue.count = 36

            val intent = Intent(context, UserDetailsMapsActivity::class.java)
            intent.putExtra("userDetailsValue", userDetailsValue as Serializable)
            context.startActivity(intent)
        }
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: UserListAdapter.ViewHolder, position: Int) {
        holder.bindItems(userList[position])
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return userList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var isImageFitToScreen: Boolean = false

        fun bindItems(user: UserData) {
            val tv_name = itemView.findViewById(R.id.tv_name) as TextView
            val tv_address = itemView.findViewById(R.id.tv_address) as TextView
            val tv_full_address = itemView.findViewById(R.id.tv_full_address) as TextView
            val iv_user = itemView.findViewById(R.id.iv_user) as ImageView

            tv_name.text = user.full_name
            tv_address.text = user.name_of_streetlocation

            val builder = SpannableStringBuilder()

            val str1 = SpannableString("Nearest Landmark - ")
            val str1_val = user.nearest_landmark + " | "
            val str1_val_span = SpannableString(str1_val.toString())

            val str2 = SpannableString("Ward No. - ")
            val str2_val = user.ward_no + " | "
            val str2_val_span = SpannableString(str2_val.toString())

            val str3 = SpannableString("Municipality - ")
            val str3_val = user.name_of_municipality
            val str3_val_span = SpannableString(str3_val.toString())

            str1.setSpan(ForegroundColorSpan(Color.parseColor("#000000")), 0, str1.toString().length, 0)
            builder.append(str1)

            str1_val_span.setSpan(ForegroundColorSpan(Color.parseColor("#696969")), 0, str1_val_span.toString().length, 0)
            builder.append(str1_val_span)

            str2.setSpan(ForegroundColorSpan(Color.parseColor("#000000")), 0, str2.toString().length, 0)
            builder.append(str2)

            str2_val_span.setSpan(ForegroundColorSpan(Color.parseColor("#696969")), 0, str2_val_span.toString().length, 0)
            builder.append(str2_val_span)

            str3.setSpan(ForegroundColorSpan(Color.parseColor("#000000")), 0, str3.toString().length, 0)
            builder.append(str3)

            str3_val_span.setSpan(ForegroundColorSpan(Color.parseColor("#696969")), 0, str3_val_span.toString().length, 0)
            builder.append(str3_val_span)

            tv_full_address.setText(builder, TextView.BufferType.SPANNABLE)

            var imgUrl: String = AppConstant().baseUrl+"public/uploads/homeless_people/borough_" + user.borough_no + "/photo/ward_" + user.ward_no + "/" + user.photo_1 + ".jpg"
            Log.v("imgUrl", "::" + imgUrl)

            Picasso.with(itemView.context)
                    .load(imgUrl)
                    .placeholder(R.drawable.round_user)
                    .error(R.drawable.round_user)
                    .into(iv_user)


            iv_user.setOnClickListener {
                val dialog = Dialog(itemView.context, R.style.AppTheme)
                dialog.setContentView(R.layout.frame_fullscreen)
                val iv_photo = dialog.findViewById(R.id.iv_photo) as ImageView
                val iv_cross = dialog.findViewById(R.id.iv_cross) as ImageView
                Picasso.with(itemView.context)
                        .load(imgUrl)
                        .placeholder(R.drawable.round_user)
                        .error(R.drawable.round_user)
                        .into(iv_photo)

                iv_cross.setOnClickListener {
                    dialog.dismiss()
                }
                dialog.show()

            }
        }
    }

    fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
        itemView.setOnClickListener {
            event.invoke(getAdapterPosition(), getItemViewType())
        }
        return this
    }
}