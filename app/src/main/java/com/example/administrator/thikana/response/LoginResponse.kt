package com.example.administrator.thikana.response

import com.google.gson.annotations.SerializedName
import java.nio.file.attribute.UserDefinedFileAttributeView

/**
 * Entire search result data class
 */
data class LoginResult(
        @SerializedName("details") val details: String,
        @SerializedName("success_bool") val success_bool: Number=0,
        @SerializedName("message") val message: String ="",
        @SerializedName("success") val success: String="")
