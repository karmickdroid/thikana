package com.example.administrator.thikana.response

import com.example.administrator.thikana.datamodel.UserData
import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by administrator on 21/3/18.
 */

class UserList : Serializable {

    @SerializedName("details")
    val userList: List<UserData> = listOf()
    @SerializedName("total_record")
    val total_record: Int = 0
    @SerializedName("success_bool")
    val success_bool: Number = 0
    @SerializedName("message")
    val message: String = ""
    @SerializedName("success")
    val success: String = ""

}