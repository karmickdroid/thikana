package com.example.administrator.thikana.utils

import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.PorterDuff
import android.graphics.drawable.ColorDrawable
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.widget.ProgressBar
import com.example.administrator.thikana.R

/**
 * Created by Pratim on 21/3/18.
 */
class MyDialog {
    internal var progressDoalog: ProgressDialog? = null

    fun showProgressLoader(ctx: Context, msg: String) {
        progressDoalog = ProgressDialog(ctx, R.style.MyProgressDialogTheme)
        //progressDoalog.setMessage(msg);
        progressDoalog!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        val drawable = ProgressBar(ctx).indeterminateDrawable.mutate()
        drawable.setColorFilter(ContextCompat.getColor(ctx, R.color.colorPrimary),
                PorterDuff.Mode.SRC_IN)
        progressDoalog!!.setIndeterminateDrawable(drawable)
        progressDoalog!!.getWindow()!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDoalog!!.show()
    }

    fun dismissProgressLoader() {
        if (progressDoalog != null && progressDoalog!!.isShowing()) {
            progressDoalog!!.dismiss()
        }
    }

    fun logoutPopup(ctx: Context, msg: String,logoutListener: LogoutListener) {
        val builder = AlertDialog.Builder(ctx)
        builder.setTitle("Logout")
        builder.setMessage("Do you want to logout?")
                .setCancelable(false)
                .setPositiveButton("Yes") { dialog, id ->
                    //do things
                    dialog.dismiss()
                    logoutListener.onOkClickListener()
                }
                .setNegativeButton("No"){dialog, id ->
                    dialog.dismiss()
                }
        val alert = builder.create()
        alert.show()
        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.parseColor("#696969"))
        alert.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.parseColor("#2196f3"))
    }

    interface LogoutListener{
        fun onOkClickListener()
    }
}