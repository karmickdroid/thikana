package com.example.administrator.thikana.datamodel

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by administrator on 22/3/18.
 */
class UserData : Serializable {

    @SerializedName("id")
    val id: String =""
    @SerializedName("status")
    val status: String =""
    @SerializedName("created_at")
    val created_at: String =""
    @SerializedName("updated_at")
    val updated_at: String =""
    @SerializedName("latitude")
    val latitude: String =""
    @SerializedName("longitude")
    val longitude: String =""
    @SerializedName("name_of_municipality")
    val name_of_municipality: String =""
    @SerializedName("ward_no")
    val ward_no: String =""
    @SerializedName("form_no")
    val form_no: String =""
    @SerializedName("name_of_streetlocation")
    val name_of_streetlocation: String =""
    @SerializedName("nearest_landmark")
    val nearest_landmark: String =""
    @SerializedName("full_name_of_respondent")
    val full_name_of_respondent: String =""
    @SerializedName("fatherhusbands_name")
    val fatherhusbands_name: String =""
    @SerializedName("present_living_status")
    val present_living_status: String =""
    @SerializedName("present_living_status_description")
    val present_living_status_description: String =""
    @SerializedName("place_of_origin")
    val place_of_origin: String =""
    @SerializedName("reasons_of_migration_from_place_of_origin")
    val reasons_of_migration_from_place_of_origin: String =""
    @SerializedName("living_in_static_location")
    val living_in_static_location: String =""
    @SerializedName("living_since")
    val living_since: String =""
    @SerializedName("whether_bplvoter_aadharany_photo_id_card_holder_govt_issued")
    val whether_bplvoter_aadharany_photo_id_card_holder_govt_issued: String =""
    @SerializedName("id_card_number")
    val id_card_number: String =""
    @SerializedName("whether_any_type_of_ration_card_holder")
    val whether_any_type_of_ration_card_holder: String =""

    @SerializedName("ration_card_number")
    val ration_card_number: String =""
    @SerializedName("whether_willing_to_avail_of_govt_arranged_shelter")
    val whether_willing_to_avail_of_govt_arranged_shelter: String =""
    @SerializedName("if_no_please_write_reason")
    val if_no_please_write_reason: String =""
    @SerializedName("full_name")
    val full_name: String =""
    @SerializedName("photo_1")
    val photo_1: String =""
    @SerializedName("photo_2")
    val photo_2: String =""
    @SerializedName("photo_3")
    val photo_3: String =""
    @SerializedName("relationship_with_respondent")
    val relationship_with_respondent: String =""
    @SerializedName("age")
    val age: String =""
    @SerializedName("sex")
    val sex: String =""


    @SerializedName("marital_status")
    val marital_status: String =""
    @SerializedName("physically_challenged")
    val physically_challenged: String =""
    @SerializedName("mentally_challenged")
    val mentally_challenged: String =""
    @SerializedName("litarate")
    val litarate: String =""
    @SerializedName("educational_qualification")
    val educational_qualification: String =""
    @SerializedName("occupation")
    val occupation: String =""
    @SerializedName("monthly_income")
    val monthly_income: String =""
    @SerializedName("personal_identification_mark")
    val personal_identification_mark: String =""
    @SerializedName("place_of_occupation")
    val place_of_occupation: String =""
    @SerializedName("habits")
    val habits: String =""

    @SerializedName("reason_for_stay_in_kmc")
    val reason_for_stay_in_kmc: String =""
    @SerializedName("zone_no")
    val zone_no: String =""
    @SerializedName("borough_no")
    val borough_no: String =""
    @SerializedName("homeless_status")
    val homeless_status: String=""
}