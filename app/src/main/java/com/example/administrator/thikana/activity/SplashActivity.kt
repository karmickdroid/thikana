package com.example.administrator.thikana.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import android.view.animation.Animation
import com.example.administrator.thikana.R
import com.example.administrator.thikana.SearchActivity
import io.paperdb.Paper

/**
 * Created by administrator on 6/3/18.
 */
class SplashActivity : Activity() {

    private lateinit var animSlideIn: Animation

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash)

        Paper.init(this)

        val background = object : Thread() {
            override fun run() {
                try {
                    // Thread will sleep for 5 seconds
                    Thread.sleep((2 * 1000).toLong())

                    // After 2 seconds redirect to another intent
                    if(Paper.book().exist("isLogin")) {
                        val i = Intent(baseContext, SearchActivity::class.java)
                        startActivity(i)
                    }
                    else{
                        val i = Intent(baseContext, LoginActivity::class.java)
                        startActivity(i)
                    }
                    //Remove activity
                    finish()
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }
        // start thread
        background.start()


    }
}