package com.example.administrator.thikana.response

import com.google.gson.annotations.SerializedName

/**
 * Created by administrator on 22/3/18.
 */
data class SearchFilter(

        @SerializedName("gender") val genderArr: List<Gender> = listOf(),
        @SerializedName("municipality") val municipalityArr: List<Municipality> = listOf(),
        @SerializedName("staticLocation") val staticLocationArr: List<StaticLocation> = listOf(),
        @SerializedName("presentLiving") val presentLivingArr: List<PresentLiving> = listOf(),
        @SerializedName("whetherAnycard") val whetherAnycardArr: List<WhetherAnycard> = listOf(),
        @SerializedName("maritalStatus") val maritalStatusArr: List<MaritalStatus> = listOf(),

        @SerializedName("physicallyChallenged") val physicallyChallengedArr: List<PhysicallyChallenged> = listOf(),
        @SerializedName("mentallyChallenged") val mentallyChallengedArr: List<MentallyChallenged> = listOf(),
        @SerializedName("respondentRelationship") val respondentRelationshipArr: List<RespondentRelationship> = listOf(),
        @SerializedName("literate") val literateArr: List<Literate> = listOf(),

        @SerializedName("govtShelter") val govtShelterArr: List<GovtShelter> = listOf(),
        @SerializedName("rationcardHolder") val rationcardHolderArr: List<RationcardHolder> = listOf(),
        @SerializedName("zoneNumber") val zoneNumberArr: List<ZoneNumber> = listOf(),
        @SerializedName("boroughNumber") val boroughNumberArr: List<BoroughNumber> = listOf(),

        @SerializedName("success_bool") val success_bool: Number = 0,
        @SerializedName("message") val message: String = "",
        @SerializedName("success") val success: String = ""
)

//Gender
data class Gender(
        @SerializedName("gender_code") val gender_code: String,
        @SerializedName("gender_name") val gender_name: String,
        var isSelected:Boolean=false
)

//Municipality
data class Municipality(
        @SerializedName("municipality_code") val municipality_code: String,
        @SerializedName("municipality_name") val municipality_name: String,
        var isSelected:Boolean=false
)

//StaticLocation
data class StaticLocation(
        @SerializedName("static_location_code") val static_location_code: String,
        @SerializedName("static_location_name") val static_location_name: String,
        var isSelected:Boolean=false
)

//PresentLiving
data class PresentLiving(
        @SerializedName("present_living_code") val present_living_code: String,
        @SerializedName("present_living_name") val present_living_name: String,
        var isSelected:Boolean=false
)

//WhetherAnycard
data class WhetherAnycard(
        @SerializedName("whether_anycard_code") val whether_anycard_code: String,
        @SerializedName("whether_anycard_name") val whether_anycard_name: String,
        var isSelected:Boolean=false
)

//MaritalStatus
data class MaritalStatus(
        @SerializedName("marital_status_code") val marital_status_code: String,
        @SerializedName("marital_status_name") val marital_status_name: String,
        var isSelected:Boolean=false
)

//PhysicallyChallenged
data class PhysicallyChallenged(
        @SerializedName("physically_challenged_code") val physically_challenged_code: String,
        @SerializedName("physically_challenged_name") val physically_challenged_name: String,
        var isSelected:Boolean=false
)

//MentallyChallenged
data class MentallyChallenged(
        @SerializedName("mentally_challenged_code") val mentally_challenged_code: String,
        @SerializedName("mentally_challenged_name") val mentally_challenged_name: String,
        var isSelected:Boolean=false
)

//RespondentRelationship
data class RespondentRelationship(
        @SerializedName("respondent_relationship_code") val respondent_relationship_code: String,
        @SerializedName("respondent_relationship_name") val respondent_relationship_name: String,
        var isSelected:Boolean=false
)

//Literate
data class Literate(
        @SerializedName("litarate_code") val litarate_code: String,
        @SerializedName("litarate_name") val litarate_name: String,
        var isSelected:Boolean=false
)

//GovtShelter
data class GovtShelter(
        @SerializedName("govt_shelter_code") val govt_shelter_code: String,
        @SerializedName("govt_shelter_name") val govt_shelter_name: String,
        var isSelected:Boolean=false
)

//RationcardHolder
data class RationcardHolder(
        @SerializedName("rationcard_holder_code") val rationcard_holder_code: String,
        @SerializedName("rationcard_holder_name") val rationcard_holder_name: String,
        var isSelected:Boolean=false
)

//ZoneNumber
data class ZoneNumber(
        @SerializedName("zone_number_code") val zone_number_code: String,
        @SerializedName("zone_number_name") val zone_number_name: String,
        var isSelected:Boolean=false
)

//BoroughNumber
data class BoroughNumber(
        @SerializedName("borough_number_code") val borough_number_code: String,
        @SerializedName("borough_number_name") val borough_number_name: String,
        var isSelected:Boolean=false
)


