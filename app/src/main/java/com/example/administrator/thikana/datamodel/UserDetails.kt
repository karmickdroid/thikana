package com.example.administrator.thikana.datamodel

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by administrator on 22/3/18.
 */
class UserDetails : Serializable {

    var nearest_landmark: String = "Nearest Landmark"
    var ward_no: String = "Ward No."
    var father_husband_name: String = "Father/Husband Name"
    var present_living_status_desc: String = "Present Living Status Description"
    var reasons_of_migration: String = "Reasons of Migration from place of origin"
    var name_of_municipality: String = "Name of Municipality"
    var form_no: String = "Form No."
    var present_living_status = "Present Living Status"
    var place_of_origin = "Place of Origin"
    var latitude = "Latitude"
    var longitude = "Longitude"
    var full_name = "Full Name"
    var borough_no = "Borough No"
    var photo_1 = ""


    var name_of_streetlocation = "Name of Street Location"
    var present_living_status_description = "Present Living Status Description"
    var living_in_static_location = "Living in static location"
    var living_since = "Living Since"
    var whether_bplvoter_aadharany_photo_id_card_holder_govt_issued = "Whether any photo_id card holder issued by govt"
    var id_card_number = "ID Card No"
    var whether_any_type_of_ration_card_holder = "Whether any type of ration card holder"
    var ration_card_number = "Ration Card No"
    var whether_willing_to_avail_of_govt_arranged_shelter = "Whether willing to avail any of govt arranged shelter"
    var full_name_of_respondent = "Full name of respondent"
    var relationship_with_respondent = "Relationship with respondent"
    var age = "Age"
    var sex = "Sex"

    var marital_status = "Marital Status"
    var physically_challenged = "Physically Challenged"

    var mentally_challenged = "Mentally Challenged"
    var litarate = "Literate"
    var educational_qualification = "Educational Qualification"
    var occupation = "Occupation"
    var monthly_income = "Monthly Income"
    var personal_identification_mark = "Personal identification mark"
    var place_of_occupation = "Place of occupation"
    var habits = "Habits"

    var reason_for_stay_in_kmc = "Reason for stay in kmc"
    var zone_no = "Zone No"
    var homeless_status = "Homeless Status"


    var count: Int = 35

}