package com.example.administrator.thikana.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.administrator.thikana.R

/**
 * Created by administrator on 21/3/18.
 */
class UserDetailsAdapter(context: Context, userData_head: Array<String>, userData_value: Array<String>, count: Int)
    : RecyclerView.Adapter<UserDetailsAdapter.ViewHolder>() {
    var context: Context = context
    var userData_head: Array<String> = userData_head
    var userData_value: Array<String> = userData_value
    var count: Int = count

    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserDetailsAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_details, parent, false)

        return ViewHolder(v).listen { pos, type ->

        }
    }

    //this method is binding the data on the list
    override fun onBindViewHolder(holder: UserDetailsAdapter.ViewHolder, position: Int) {
        holder.bindItems(userData_value[position], userData_head[position])
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return count
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(value: String, head: String) {
            val tv_head = itemView.findViewById(R.id.tv_head) as TextView
            val tv_subhead = itemView.findViewById(R.id.tv_subhead) as TextView

            tv_head.text = head
            tv_subhead.text = value
        }
    }

    fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
        itemView.setOnClickListener {
            event.invoke(getAdapterPosition(), getItemViewType())
        }
        return this
    }
}