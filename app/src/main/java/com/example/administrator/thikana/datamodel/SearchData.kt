package com.example.administrator.thikana.datamodel

import com.example.administrator.thikana.retrofit.RepositoryProvider
import kotlinx.android.synthetic.main.activity_search.*
import java.io.Serializable

/**
 * Created by administrator on 23/3/18.
 */
class SearchData : Serializable {

    var name_of_municipality: String = ""
    var ward_no: String = ""
    var name_of_streetlocation: String = ""
    var nearest_landmark: String = ""
    var full_name_of_respondent: String = ""
    var present_living_status: String = ""
    var place_of_origin: String = ""
    var reasons_of_migration_from_place_of_origin: String = ""
    var whether_bplvoter_aadharany_photo_id_card_holder_govt_issued: String = ""
    var whether_willing_to_avail_of_govt_arranged_shelter: String = ""
    var full_name: String = ""
}