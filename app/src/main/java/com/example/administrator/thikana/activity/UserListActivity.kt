package com.example.administrator.thikana.activity

import android.content.Intent
import android.os.Bundle
import android.provider.SyncStateContract
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.LinearLayout
import android.widget.Toast
import com.example.administrator.thikana.R
import com.example.administrator.thikana.adapter.UserListAdapter
import com.example.administrator.thikana.datamodel.SearchData
import com.example.administrator.thikana.datamodel.UserData
import com.example.administrator.thikana.retrofit.RepositoryProvider
import com.example.administrator.thikana.utils.MyDialog
import io.paperdb.Paper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.activity_userlist.*
import kotlinx.android.synthetic.main.my_toolbar.*
import java.io.Serializable
import android.nfc.tech.MifareUltralight.PAGE_SIZE
import android.support.v4.widget.SwipeRefreshLayout
import android.util.Log
import android.view.View
import com.example.administrator.thikana.utils.AppUtils


/**
 * Created by administrator on 21/3/18.
 */
class UserListActivity : AppCompatActivity(), MyDialog.LogoutListener {

    override fun onOkClickListener() {
        callLogout()
    }

    var users: ArrayList<UserData>? = null
    var listAdapter: UserListAdapter? = null
    val compositeDisposable: CompositeDisposable = CompositeDisposable()
    lateinit var searchData: SearchData
    var mEndOfList = 100
    var mCurrentPage = 0
    var length = 10
    var totDataPopulated = 0
    val mDialog: MyDialog = MyDialog()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_userlist)
        Paper.init(this)

        //adding a layoutmanager
        recyclerView.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false) as RecyclerView.LayoutManager?
        recyclerView.addItemDecoration(LinearLayoutSpaceItemDecoration(this))


        //creating an arraylist to store users using the data class user
        users = ArrayList<UserData>()
        searchData = intent.extras.get("searchData") as SearchData

        //creating our listAdapter
        listAdapter = UserListAdapter(this, users!!)

        //now adding the listAdapter to recyclerview
        recyclerView.adapter = listAdapter

        mDialog.showProgressLoader(this, "Fetching..")
        ivBackToolbar.setOnClickListener {
            finish()
        }
        ivLogoutToolbar.setOnClickListener {
            val mDialog: MyDialog = MyDialog()
            mDialog.logoutPopup(this, "Logout", this)
        }

        if (AppUtils().isNetworkAvailable(this))
            callUserListApi()
        else
            Toast.makeText(this, "No Internet Connection!!!", Toast.LENGTH_LONG).show()


        swipe_refresh.setOnRefreshListener(SwipeRefreshLayout.OnRefreshListener {
            mCurrentPage = 0
            users!!.clear()
            swipe_refresh.setRefreshing(true)
            if (AppUtils().isNetworkAvailable(this))
                callUserListApi()
            else
                Toast.makeText(this, "No Internet Connection!!!", Toast.LENGTH_LONG).show()
        })
        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView?, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                val visibleItemCount = recyclerView!!.layoutManager.childCount
                val totalItemCount = recyclerView.layoutManager.itemCount
                val firstVisibleItemPosition = (recyclerView.layoutManager as LinearLayoutManager).findFirstVisibleItemPosition()


                if (!swipe_refresh.isRefreshing() && totalItemCount <= mEndOfList) {
                    if (visibleItemCount + firstVisibleItemPosition >= totalItemCount - 1
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= 10) {
                        Log.v("ppp", "totalitemCount" + totalItemCount)
                        onLoadNextPage()
                    }
                }
            }
        })

    }

    private fun callLogout() {
        Paper.book().delete("isLogin")
        val i = Intent(this, LoginActivity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(i)
        this.finish()

    }

    fun onLoadNextPage() {
        swipe_refresh.setRefreshing(true)
        mCurrentPage += 10
        Log.v("ppp", "mCurrentPage" + mCurrentPage)
        if (AppUtils().isNetworkAvailable(this))
            callUserListApi()
        else
            Toast.makeText(this, "No Internet Connection!!!", Toast.LENGTH_LONG).show()
    }

    private fun callUserListApi() {

        val repository = RepositoryProvider.provideRepository()
        var name_of_municipality = searchData.name_of_municipality
        var ward_no = searchData.ward_no
        var name_of_streetlocation = searchData.name_of_streetlocation
        var nearest_landmark = searchData.nearest_landmark
        var full_name_of_respondent = searchData.full_name_of_respondent
        var present_living_status = searchData.present_living_status
        var place_of_origin = searchData.place_of_origin
        var reasons_of_migration_from_place_of_origin = searchData.reasons_of_migration_from_place_of_origin
        var whether_bplvoter_aadharany_photo_id_card_holder_govt_issued = searchData.whether_bplvoter_aadharany_photo_id_card_holder_govt_issued
        var whether_willing_to_avail_of_govt_arranged_shelter = searchData.whether_willing_to_avail_of_govt_arranged_shelter
        var full_name = searchData.full_name
        compositeDisposable.add(
                repository.getUsers(name_of_municipality,
                        ward_no,
                        name_of_streetlocation,
                        nearest_landmark,
                        full_name_of_respondent,
                        present_living_status,
                        place_of_origin,
                        reasons_of_migration_from_place_of_origin,
                        whether_bplvoter_aadharany_photo_id_card_holder_govt_issued,
                        whether_willing_to_avail_of_govt_arranged_shelter,
                        full_name, mCurrentPage, length)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeOn(Schedulers.io())
                        .subscribe({ result ->
                            mEndOfList = result.total_record
                            if (totDataPopulated < mEndOfList)
                                totDataPopulated += result.userList.size

                            Log.v("ppp", "totDataPopulated" + totDataPopulated)
                            Log.v("ppp", "mEndOfList" + mEndOfList)
                            swipe_refresh.setRefreshing(false)
                            mDialog.dismissProgressLoader()

                            val userDataList: List<UserData> = result.userList

                            if (userDataList!!.isNotEmpty()) {
                                tvNoDataFound.visibility = View.GONE
                                users?.addAll(userDataList)
                            } else {
                                if (users!!.isEmpty()) {
                                    tvNoDataFound.visibility = View.VISIBLE
                                    Toast.makeText(this, "No Data!!!!", Toast.LENGTH_LONG).show()
                                }
                            }

                            listAdapter!!.notifyDataSetChanged()

                        }, { error ->
                            swipe_refresh.setRefreshing(false)
                            mDialog.dismissProgressLoader()
                            error.printStackTrace()
                        })
        )
    }


}